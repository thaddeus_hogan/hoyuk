#ifndef QUADCOORDS_H
#define QUADCOORDS_H

#include <assert.h>
#include <GL/glew.h>
#include <angelscript.h>

class QuadCoords {
public:
    static void ASRegister(asIScriptEngine *scr) {
        int r;
        r = scr->RegisterObjectType("QuadCoords", sizeof(QuadCoords), asOBJ_VALUE | asOBJ_POD); assert (r >= 0);
        r = scr->RegisterObjectProperty("QuadCoords", "double top", asOFFSET(QuadCoords, top)); assert (r >= 0);
        r = scr->RegisterObjectProperty("QuadCoords", "double left", asOFFSET(QuadCoords, left)); assert (r >= 0);
        r = scr->RegisterObjectProperty("QuadCoords", "double bottom", asOFFSET(QuadCoords, bottom)); assert (r >= 0);
        r = scr->RegisterObjectProperty("QuadCoords", "double right", asOFFSET(QuadCoords, right)); assert (r >= 0);
    }

    GLdouble top;
    GLdouble left;
    GLdouble bottom;
    GLdouble right;
};

#endif // QUADCOORDS_H
