#ifndef CAMERA_H
#define CAMERA_H

#include <assert.h>
#include <GL/glew.h>
#include <angelscript.h>

#include "quadcoords.h"

class Camera {
protected:
    GLdouble _width; // meters
    GLdouble _height; // meters
    GLdouble _aspect; // width / height
    GLdouble _x; // meters
    GLdouble _y; // meters
    GLuint _pxWidth; // pixels
    GLuint _pxHeight; // pixels
    GLdouble _xScale; // pixels per meter
    GLdouble _yScale; // pixels per meter

    void computeScales();

public:
    static void ASRegister(asIScriptEngine *scr);
    static GLdouble getAspectFromPxDim(GLuint pxWidth, GLuint pxHeight);

    Camera();
    Camera(GLuint _pxWidth, GLuint _pxHeight);

    void width(GLdouble _width);
    GLdouble width();
    void height(GLdouble _height);
    GLdouble height();
    GLdouble aspect();
    void x(GLdouble _x);
    GLdouble x();
    void y(GLdouble _y);
    GLdouble y();
    GLdouble xScale();
    GLdouble yScale();

    QuadCoords rect() const;
};

#endif // CAMERA_H
