#ifndef CONTROLSET_H
#define CONTROLSET_H

#include <assert.h>
#include <angelscript.h>

class ControlSet {
protected:
    bool _btn[10];
    bool _btnLast[10];
    bool _btnChange[10];

public:
    static void ASRegister(asIScriptEngine *scr);

    static const unsigned btnCount = 10;

    signed short x;
    signed short y;
    bool btn(unsigned id);
    void btn(unsigned id, bool val);
    bool btnLast(unsigned id);
    void btnLast(unsigned id, bool val);
    bool btnChange(unsigned id);
    void btnChange(unsigned id, bool val);

    ControlSet();
};

#endif // CONTROLSET_H
