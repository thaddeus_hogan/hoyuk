#include "background.h"

Background::Background(const QString &name,
                       GLdouble width, GLdouble height,
                       GLuint texID,
                       const TextureRect &trect) {
    this->name = QString(name);
    this->width = width;
    this->height = height;
    this->texID = texID;
    this->trect = TextureRect(trect);
}

const QString& Background::getName() const {
    return name;
}

GLdouble Background::getWidth() {
    return width;
}

GLdouble Background::getHeight() {
    return height;
}

GLuint Background::getTexID() {
    return texID;
}

const TextureRect& Background::getTextureRect() const {
    return trect;
}

void Background::unload() {
    glDeleteTextures(1, &texID);
}

BackgroundInstance::BackgroundInstance(Background *background, GLdouble x, GLdouble y) {
    this->background = background;
    this->x = x;
    this->y = y;
}

Background* BackgroundInstance::getBackground() {
    return background;
}

QuadCoords BackgroundInstance::rect() const {
    QuadCoords qc;
    qc.top = y;
    qc.left = x;
    qc.bottom = y + background->getHeight();
    qc.right = x + background->getWidth();

    return qc;
}
