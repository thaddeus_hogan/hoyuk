#include "hworld.h"

HWorld::HWorld() {
    _world = NULL;
    initialized = false;
}

HWorld::~HWorld() {
    QList<ActorInstance*>::iterator actInstIt;
    for (actInstIt = actorInstances.begin(); actInstIt != actorInstances.end(); ++actInstIt) {
        ActorInstance *actInst = *actInstIt;
        if (actInst != NULL)
            delete actInst;
    }

    if (_world != NULL)
        delete _world;
}

bool HWorld::isInitialized() {
    return initialized;
}

void HWorld::initialize() {
    _world = new b2World(b2Vec2(0, 0));
    initialized = true;
}

b2World* HWorld::world() {
    return _world;
}

void HWorld::destroy() {
    unsigned instCount = 0;
    foreach (ActorInstance *actInst, actorInstances) {
        delete actInst;
        instCount += 1;
    }

    qDebug() << "HWorld destroyed " << instCount << " actor instances";

    actorInstances.clear();
    backgroundInstances.clear();

    if (_world != NULL)
        delete _world;
    _world = NULL;

    qDebug() << "HWorld destroyed Box2D world";

    initialized = false;
}

void HWorld::ASRegister(asIScriptEngine *scr) {
    // Register Hoyuk World Model Types
    QuadCoords::ASRegister(scr);
    Camera::ASRegister(scr);
    ControlSet::ASRegister(scr);
    Actor::ASRegister(scr);
    ActorInstance::ASRegister(scr);
}
