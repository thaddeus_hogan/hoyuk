#ifndef ACTOR_H
#define ACTOR_H

#include <QtCore>
#include <GL/glew.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>

#include "../scriptutil.h"
#include "animationstate.h"
#include "quadcoords.h"
#include "controlset.h"

#define ASFD_ACTOR_ON_LOOP "void onLoop(ActorInstance@, ControlSet@, float)"

class ActorInstance;

class Actor {
private:
    QString name;

    GLdouble width;
    GLdouble height;

    bool directionalX;
    bool directionalY;

    // QuadCoords contains coordinates about the actor's center
    // rather than from the upper left
    QuadCoords qCoords;

    QString defaultState;
    QHash<QString, AnimationState> animationStates;

    b2BodyDef bodyDef;
    QList<b2FixtureDef> fixtures;

    asIScriptModule *scriptModule;
    asIScriptFunction *funcOnLoop;

public:
    friend class ActorInstance;

    static void ASRegister(asIScriptEngine *scr);

    Actor(QString name);
    ~Actor();

    const QString& getName() const;
    void setDimensions(GLdouble width, GLdouble height);
    void setDefaultState(const QString &defaultState);
    void setAnimationState(const QString &state, const AnimationState &animState);
    const QuadCoords& getQuadCoords() const;
    GLdouble getWidth();
    GLdouble getHeight();
    bool getDirectionalX();
    void setDirectionalX(bool directionalX);
    bool getDirectionalY();
    void setDirectionalY(bool directionalY);
    const QString& getDefaultState() const;
    AnimationState getAnimationState(const QString &state) const;

    b2BodyDef* bodyDefPtr();
    void setBodyDef(b2BodyDef bodyDef);
    QList<b2FixtureDef>& getFixtures();

    asIScriptModule* getScriptModule();
    void setScriptModule(asIScriptModule *scriptModule);
};

class ActorInstance {

protected:
    Actor *actor;
    unsigned _instID;

    QString animStateName;
    AnimationState animState;
    uint frameNum;
    uint frameTicks;
    uint ticksPerFrame;

    // Cannot be de-allocted by destructor, must be cleaned up in SDLThread/HWorld
    b2Body *body;

    // Store a copy of the body definitions for use when passivated
    b2BodyDef bodyDef;
    QList<b2FixtureDef> fixtures;

    static const AnimationState nullState;

public:
    float32 lastDirX;
    float32 lastDirY;
    unsigned controlSet;
    bool null;

    ActorInstance();
    ActorInstance(Actor *actor, unsigned _instID);

    bool isNull() const;

    Actor* getActor();

    /*!
     * \brief createBody Create a box2d body in the provided world using this instance's stored body definition
     * \param world b2world in which to create body
     * \return true if created, false if body is a NULL pointer
     */
    bool createBody(b2World *world);

    bool hasBody() const;
    b2Body& bodyRef();

    b2Body* bodyPtr();
    b2BodyDef* bodyDefPtr();

    /*!
     * \brief setState Set the animation state to the provided value, also resets the animation frame to 0
     */
    void setState(const QString &state);

    /*!
     * \brief changeState Set the animation state to the provided value, if it is not already the specified state
     */
    void changeState(const QString &state);

    const AnimationState& getAnimationState();
    const QString &getAnimationStateName();
    GLuint getTexID();
    const QString& name() const;
    unsigned instID() const;
    void setFriction(float32 friction);

    void onLoop(float32 timeStep, ControlSet *cset);

    static ActorInstance NullActorInstance;
    static void ASRegister(asIScriptEngine *scr);
};

#endif // ACTOR_H
