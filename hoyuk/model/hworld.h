#ifndef HWORLD_H
#define HWORLD_H

#include <assert.h>
#include <QtCore>
#include <Box2D/Box2D.h>
#include <angelscript.h>

#include "actor.h"
#include "background.h"
#include "camera.h"
#include "quadcoords.h"
#include "texturerect.h"
#include "animationstate.h"
#include "controlset.h"

class HWorld {

protected:
    bool initialized;
    b2World *_world;

public:
    static void ASRegister(asIScriptEngine *scr);

    QList<ActorInstance*> actorInstances;
    QList<BackgroundInstance> backgroundInstances;

    HWorld();
    ~HWorld();
    bool isInitialized();
    void initialize();
    void destroy();

    b2World* world();
};

#endif // HWORLD_H
