#ifndef TEXTURERECT_H
#define TEXTURERECT_H

#include <GL/glew.h>

class TextureRect {
public:
    /*!
     * \brief TextureRect Creates texture rect and sets all values to 0
     */
    TextureRect();

    /*!
     * \brief TextureRect Create a TextureRect using pixel values that will be converted into GLdouble ratios
     *
     * \param pxres Resolution of texture, assumed to be square
     * \param pxtop Upper-most pixel to render
     * \param pxleft Left-most pixel to render
     * \param pxbottom Bottom-most pixel to render
     * \param pxright Right-most pixel to render
     * \param invertY If true (default), invert the Y axis of the rect (required for raw data from decoded PNG)
     */
    TextureRect(int pxres, int pxtop, int pxleft, int pxbottom, int pxright, bool invertY = false);

    TextureRect(const TextureRect &other);

    GLdouble top;
    GLdouble left;
    GLdouble bottom;
    GLdouble right;
};

#endif // TEXTURERECT_H
