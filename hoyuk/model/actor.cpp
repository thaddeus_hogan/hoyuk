#include "model\actor.h"

Actor::Actor(QString name) {
    this->name = name;
    width = 0;
    height = 0;
    directionalX = false;
    directionalY = false;
    defaultState = QString();
    animationStates = QHash<QString, AnimationState>();
    scriptModule = NULL;
    funcOnLoop = NULL;
}

Actor::~Actor() {
    foreach (QString stateName, animationStates.keys()) {
        animationStates[stateName].unload();
    }

    animationStates.clear();

    foreach (b2FixtureDef fixDef, fixtures) {
        if (fixDef.shape != NULL)
            delete fixDef.shape;
    }

    fixtures.clear();
}

const QString& Actor::getName() const {
    return name;
}

void Actor::setDimensions(GLdouble width, GLdouble height) {
    this->width = width;
    this->height = height;

    qCoords.top = (height / 2.0) * -1;
    qCoords.left = (width / 2.0) * -1;
    qCoords.bottom = height / 2.0;
    qCoords.right = width / 2.0;
}

void Actor::setDefaultState(const QString &defaultState) {
    this->defaultState = QString(defaultState);
}

void Actor::setAnimationState(const QString &state, const AnimationState &animState) {
    animationStates.insert(state, animState);
}

const QuadCoords& Actor::getQuadCoords() const {
    return qCoords;
}

GLdouble Actor::getWidth() {
    return width;
}

GLdouble Actor::getHeight() {
    return height;
}

bool Actor::getDirectionalX() {
    return directionalX;
}

void Actor::setDirectionalX(bool directionalX) {
    this->directionalX = directionalX;
}

bool Actor::getDirectionalY() {
    return directionalY;
}

void Actor::setDirectionalY(bool directionalY) {
    this->directionalY = directionalY;
}

const QString& Actor::getDefaultState() const {
    return defaultState;
}

AnimationState Actor::getAnimationState(const QString &state) const {
    return animationStates.value(state);
}

b2BodyDef* Actor::bodyDefPtr() {
    return &bodyDef;
}

void Actor::setBodyDef(b2BodyDef bodyDef) {
    this->bodyDef = bodyDef;
}

QList<b2FixtureDef> &Actor::getFixtures() {
    return fixtures;
}

asIScriptModule* Actor::getScriptModule() {
    return scriptModule;
}

void Actor::setScriptModule(asIScriptModule *scriptModule) {
    this->scriptModule = scriptModule;
    if (scriptModule != NULL)
        funcOnLoop = scriptModule->GetFunctionByDecl(ASFD_ACTOR_ON_LOOP);
}

void Actor::ASRegister(asIScriptEngine *scr) {
    int r;
    r = scr->RegisterObjectType("Actor", 0, asOBJ_REF | asOBJ_NOCOUNT); assert(r >= 0);

    r = scr->RegisterObjectMethod("Actor", "const string &getName() const", asMETHOD(Actor, getName), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Actor", "void setDimensions(double, double)", asMETHOD(Actor, setDimensions), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Actor", "const QuadCoords &getQuadCoords() const", asMETHOD(Actor, getQuadCoords), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("Actor", "double getWidth()", asMETHOD(Actor, getWidth), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("Actor", "double getHeight()", asMETHOD(Actor, getHeight), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("Actor", "bool getDirectionalX()", asMETHOD(Actor, getDirectionalX), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("Actor", "bool getDirectionalY()", asMETHOD(Actor, getDirectionalY), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("Actor", "const string &getDefaultState() const", asMETHOD(Actor, getDefaultState), asCALL_THISCALL); assert (r >= 0);
    //r = scr->RegisterObjectMethod("Actor", "", asMETHOD(Actor, ), asCALL_THISCALL); assert (r >= 0);
}

ActorInstance::ActorInstance() {
    this->null = true;
}

ActorInstance::ActorInstance(Actor *actor, unsigned _instID) {
    this->null = false;
    this->actor = actor;
    this->_instID = _instID;

    // Copy body definitions
    this->body = NULL;
    this->bodyDef = *(actor->bodyDefPtr());

    foreach (b2FixtureDef fixDef, actor->fixtures) {
        fixtures.append(fixDef);
    }

    controlSet = 0;

    if (actor->directionalX)
        lastDirX = 1;
    if (actor->directionalY)
        lastDirY = 1;

    animState = actor->getAnimationState(actor->getDefaultState());

    frameNum = 0;
    frameTicks = 0;

    if (animState.getFrameRate() != 0) {
        ticksPerFrame = 60 / animState.getFrameRate();
    } else {
        ticksPerFrame = 1;
    }
}

bool ActorInstance::isNull() const {
    return null;
}

Actor* ActorInstance::getActor() {
    return actor;
}

bool ActorInstance::createBody(b2World *world) {
    if (body != NULL)
        return false;

    body = world->CreateBody(&bodyDef);
    foreach (b2FixtureDef fixDef, fixtures) {
        body->CreateFixture(&fixDef);
    }

    return true;
}

bool ActorInstance::hasBody() const {
    return (body != NULL);
}

b2Body& ActorInstance::bodyRef() {
    return *body;
}

b2Body* ActorInstance::bodyPtr() {
    return body;
}

b2BodyDef* ActorInstance::bodyDefPtr() {
    return &bodyDef;
}

void ActorInstance::setState(const QString &state) {
    animState = actor->getAnimationState(state);
    animStateName = QString(state);

    frameNum = 0;
    frameTicks = 0;

    if (animState.getFrameRate() != 0) {
        ticksPerFrame = 60 / animState.getFrameRate();
    } else {
        ticksPerFrame = 1;
    }
}

void ActorInstance::changeState(const QString &state) {
    if (animStateName.compare(state) != 0)
        setState(state);
}

void ActorInstance::onLoop(float32 timeStep, ControlSet *cset) {
    if (animState.getFrameRate() > 0) {
        frameTicks += 1;
        if (frameTicks == ticksPerFrame) {
            frameNum += 1;
            frameTicks = 0;

            if (frameNum == animState.getFrameCount()) {
                frameNum = 0;
            }
        }
    }

    if (actor->funcOnLoop != NULL) {
        asIScriptContext *scrCtx = ScriptUtil::instance().scrCtx();
        scrCtx->Prepare(actor->funcOnLoop);
        scrCtx->SetArgAddress(0, this);
        scrCtx->SetArgAddress(1, cset);
        scrCtx->SetArgFloat(2, timeStep);

        if (scrCtx->Execute() != asEXECUTION_FINISHED) {
            qDebug() << "ActorInstance script onLoop() execution failed for " << name() << " instance " << instID();
            actor->funcOnLoop = NULL;
        }
    }
}

GLuint ActorInstance::getTexID() {
    return animState.getTexID(frameNum);
}

const AnimationState& ActorInstance::getAnimationState() {
    return animState;
}

const QString &ActorInstance::getAnimationStateName() {
    return animStateName;
}

void ActorInstance::setFriction(float32 friction) {
    b2Fixture *fix = bodyPtr()->GetFixtureList();
    while (fix != NULL) {
        if (fix->IsSensor() == false)
            fix->SetFriction(friction);
        fix = fix->GetNext();
    }
}

const QString& ActorInstance::name() const {
    return actor->getName();
}

unsigned ActorInstance::instID() const {
    return _instID;
}

void ActorInstance::ASRegister(asIScriptEngine *scr) {
    int r;
    r = scr->RegisterObjectType("ActorInstance", 0, asOBJ_REF | asOBJ_NOCOUNT); assert(r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "bool isNull() const", asMETHOD(ActorInstance, isNull), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "bool hasBody() const", asMETHOD(ActorInstance, hasBody), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "b2Body &bodyRef()", asMETHOD(ActorInstance, bodyRef), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "void setState(string &in)", asMETHOD(ActorInstance, setState), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "void setFriction(float)", asMETHOD(ActorInstance, setFriction), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "void changeState(string &in)", asMETHOD(ActorInstance, changeState), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "const string &getAnimationStateName()", asMETHOD(ActorInstance, getAnimationStateName), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "const string &name() const", asMETHOD(ActorInstance, name), asCALL_THISCALL); assert (r >= 0);
    r = scr->RegisterObjectMethod("ActorInstance", "uint instID() const", asMETHOD(ActorInstance, instID), asCALL_THISCALL); assert (r >= 0);

    r = scr->RegisterGlobalProperty("ActorInstance NullActorInstance", &ActorInstance::NullActorInstance);
}

ActorInstance ActorInstance::NullActorInstance;
