#include "controlset.h"

ControlSet::ControlSet() {
    x = 0;
    y = 0;

    for (unsigned i = 0; i < 10; i++) {
        _btn[i] = false;
        _btnLast[i] = false;
        _btnChange[i] = false;
    }
}

bool ControlSet::btn(unsigned id) {
    return (id <= btnCount) ? _btn[id] : false;
}

void ControlSet::btn(unsigned id, bool val) {
    if (id <= btnCount)
        _btn[id] = val;
}

bool ControlSet::btnLast(unsigned id) {
    return (id <= btnCount) ? _btnLast[id] : false;
}

void ControlSet::btnLast(unsigned id, bool val) {
    if (id <= btnCount)
        _btnLast[id] = val;
}

bool ControlSet::btnChange(unsigned id) {
    return (id <= btnCount) ? _btnChange[id] : false;
}

void ControlSet::btnChange(unsigned id, bool val) {
    if (id <= btnCount)
        _btnChange[id] = val;
}

void ControlSet::ASRegister(asIScriptEngine *scr) {
    int r;
    r = scr->RegisterObjectType("ControlSet", 0, asOBJ_REF | asOBJ_NOCOUNT); assert(r >= 0);
    r = scr->RegisterObjectProperty("ControlSet", "int16 x", asOFFSET(ControlSet, x)); assert(r >= 0);
    r = scr->RegisterObjectProperty("ControlSet", "int16 y", asOFFSET(ControlSet, y)); assert(r >= 0);
    r = scr->RegisterObjectMethod("ControlSet", "bool btn(uint)", asMETHODPR(ControlSet, btn, (unsigned), bool), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("ControlSet", "void btn(uint, bool)", asMETHODPR(ControlSet, btn, (unsigned, bool), void), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("ControlSet", "bool btnLast(uint)", asMETHODPR(ControlSet, btnLast, (unsigned), bool), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("ControlSet", "void btnLast(uint, bool)", asMETHODPR(ControlSet, btnLast, (unsigned, bool), void), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("ControlSet", "bool btnChange(uint)", asMETHODPR(ControlSet, btnChange, (unsigned), bool), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("ControlSet", "void btnChange(uint, bool)", asMETHODPR(ControlSet, btnChange, (unsigned, bool), void), asCALL_THISCALL); assert(r >= 0);
}
