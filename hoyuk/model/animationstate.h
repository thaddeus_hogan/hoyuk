#ifndef ANIMATIONSTATE_H
#define ANIMATIONSTATE_H

#include <QtCore>
#include <GL/glew.h>
#include "texturerect.h"

class AnimationState {
private:
    TextureRect texRect;
    GLuint *frameTexIDs;
    uint numFrames;
    uint frameRate;
    bool null;

public:
    AnimationState();
    AnimationState(uint numFrames, const TextureRect & texRect, uint frameRate);
    ~AnimationState();

    bool isNull();
    uint getFrameCount();
    const TextureRect& getTextureRect() const;
    void setFrameTexID(uint frameNum, GLuint texID);
    GLuint getTexID(uint frameNum);
    uint getFrameRate();
    void unload();
};

#endif // ANIMATIONSTATE_H
