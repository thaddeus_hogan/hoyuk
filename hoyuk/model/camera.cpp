#include "camera.h"

GLdouble Camera::getAspectFromPxDim(GLuint pxWidth, GLuint pxHeight) {
    return (GLdouble)pxWidth / (GLdouble)pxHeight;
}

Camera::Camera() {

}

Camera::Camera(GLuint _pxWidth, GLuint _pxHeight) {
    this->_pxWidth = _pxWidth;
    this->_pxHeight = _pxHeight;
    this->_aspect = Camera::getAspectFromPxDim(_pxWidth, _pxHeight);
    x(0);
    y(0);
    width(8.0);
}

void Camera::computeScales() {
    _xScale = (GLdouble)_pxWidth / _width;
    _yScale = (GLdouble)_pxHeight / _height;
}

void Camera::width(GLdouble _width) {
    this->_width = _width;
    _height = _width / _aspect;
    computeScales();
}

GLdouble Camera::width() {
    return _width;
}

void Camera::height(GLdouble _height) {
    this->_height = _height;
    _width = _height * _aspect;
    computeScales();
}

GLdouble Camera::height() {
    return _height;
}

GLdouble Camera::aspect() {
    return _aspect;
}

void Camera::x(GLdouble _x) {
    this->_x = _x;
}

GLdouble Camera::x() {
    return _x;
}

void Camera::y(GLdouble _y) {
    this->_y = _y;
}

GLdouble Camera::y() {
    return _y;
}

GLdouble Camera::xScale() {
    return _xScale;
}

GLdouble Camera::yScale() {
    return _yScale;
}

QuadCoords Camera::rect() const {
    QuadCoords qc;
    qc.top = _y;
    qc.left = _x;
    qc.bottom = _y + _height;
    qc.right = _x + _width;

    return qc;
}

void Camera::ASRegister(asIScriptEngine *scr) {
    int r;
    r = scr->RegisterObjectType("Camera", 0, asOBJ_REF | asOBJ_NOCOUNT);
    r = scr->RegisterObjectMethod("Camera", "void width(double)", asMETHODPR(Camera, width, (GLdouble), void), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "void height(double)", asMETHODPR(Camera, height, (GLdouble), void), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "void x(double)", asMETHODPR(Camera, x, (GLdouble), void), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "void y(double)", asMETHODPR(Camera, y, (GLdouble), void), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "double width()", asMETHODPR(Camera, width, (void), GLdouble), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "double height()", asMETHODPR(Camera, height, (void), GLdouble), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "double x()", asMETHODPR(Camera, x, (void), GLdouble), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "double y()", asMETHODPR(Camera, y, (void), GLdouble), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "double xScale()", asMETHOD(Camera, xScale), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "double yScale()", asMETHOD(Camera, yScale), asCALL_THISCALL); assert(r >= 0);
    r = scr->RegisterObjectMethod("Camera", "double aspect()", asMETHOD(Camera, aspect), asCALL_THISCALL); assert(r >= 0);
}
