#include "followxlogic.h"

FollowXLogic::FollowXLogic(Camera *camera, ActorInstance *actInst) : CameraLogic(camera) {
    this->actInst = actInst;
    xMin = 0;
    xMax = 100;
}

void FollowXLogic::operator ()() {
    GLdouble camXOffset = camera->width() / 2;
    GLdouble newX = (GLdouble)actInst->bodyPtr()->GetPosition().x - camXOffset;
    if (newX < xMin)
        newX = xMin;
    else if (newX > xMax)
        newX = xMax;

    camera->x(newX);
}
