#ifndef FOLLOWXLOGIC_H
#define FOLLOWXLOGIC_H

#include "cameralogic.h"

class FollowXLogic : public CameraLogic {
protected:
    ActorInstance *actInst;
    GLdouble xMin;
    GLdouble xMax;

public:
    FollowXLogic(Camera *camera, ActorInstance *actInst);
    void operator ()();
};

#endif // FOLLOWXLOGIC_H
