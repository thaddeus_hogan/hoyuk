#ifndef CAMERALOGIC_H
#define CAMERALOGIC_H

#include "../model/hworld.h"

class CameraLogic {
protected:
    Camera *camera;

public:
    CameraLogic(Camera *camera);
    virtual void operator ()() = 0;
};

#endif // CAMERALOGIC_H
