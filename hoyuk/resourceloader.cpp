#include "resourceloader.h"

Actor* loadActorResource(const QString &filePath) {
    ActorReader reader;
    reader.readArchive(filePath);

    if (reader.getLoadResult().errorCode != OK) {
        qDebug() << "ResourceLoader read actor resource failed: "
                 << reader.getLoadResult().errorMessage;
        return NULL;
    }

    return reader.getActor();
}

Background* loadBackgroundResource(const QString &filePath) {
    BackgroundReader reader;
    reader.readArchive(filePath);

    if (reader.getLoadResult().errorCode != OK) {
        qDebug() << "ResourceLoader read background resource failed: "
                 << reader.getLoadResult().errorMessage;
        return NULL;
    }

    return reader.getBackground();
}
