#ifndef SETGLOBALLOOPLOCKTASK_H
#define SETGLOBALLOOPLOCKTASK_H

#include "../sdlthread.h"

class SetGlobalLoopLockTask : public BlockingExtTask {
    SDLThread *sdlThread;
    bool globalLoopLock;

public:
    SetGlobalLoopLockTask(SDLThread *sdlThread, bool globalLoopLock) : BlockingExtTask("SetGlobalLoopLockTask") {
        this->sdlThread = sdlThread;
        this->globalLoopLock = globalLoopLock;
    }

    void run() {
        sdlThread->setGlobalLoopLock(globalLoopLock);
        qDebug() << "SDLThread Global Loop Lock " << (globalLoopLock ? "Enabled" : "Disabled");
    }
};

#endif // SETGLOBALLOOPLOCKTASK_H
