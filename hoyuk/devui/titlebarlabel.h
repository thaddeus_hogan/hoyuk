#ifndef TITLEBARLABEL_H
#define TITLEBARLABEL_H

#include <QtGui>

class TitleBarLabel : public QLabel {
    Q_OBJECT

protected:
    bool mouseDown;
    int xOff;
    int yOff;

    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);

public:
    explicit TitleBarLabel(QWidget *parent = 0);
    
signals:
    void titleLabelDragged(QPoint newPos);
    
public slots:
    
};

#endif // TITLEBARLABEL_H
