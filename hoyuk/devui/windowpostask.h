#ifndef WINDOWPOSTASK_H
#define WINDOWPOSTASK_H

#ifdef _WIN32
#include <Windows.h>
#endif

#include "../sdlthread.h"

class WindowPosTask : public BlockingExtTask {
protected:
    int x;
    int y;
    int width;
    int height;

    bool set;

#ifdef _WIN32
    HWND sdlWindowHandle;
    unsigned windowsFound;
#endif

public:
    WindowPosTask();
    WindowPosTask(int x, int y);
    void run();

    int getX();
    int getY();
    int getWidth();
    int getHeight();

#ifdef _WIN32
    void setSDLWindowHandle(HWND sdlWindowHandle);
#endif
};

#endif // WINDOWPOSTASK_H
