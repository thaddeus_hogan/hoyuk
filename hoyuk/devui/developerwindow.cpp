#include "devui\developerwindow.h"
#include "ui_developerwindow.h"

DeveloperWindow::DeveloperWindow(SDLThread *sdlThread) : QMainWindow(0), ui(new Ui::DeveloperWindow) {
    ui->setupUi(this);
    this->sdlThread = sdlThread;

    connect(&snapTimer, SIGNAL(timeout()), this, SLOT(onSnapTimerTimeout()));
    snapTimer.setInterval(100);
    snapTimer.start();
}

DeveloperWindow::~DeveloperWindow() {
    delete ui;
}

void DeveloperWindow::snapToSDLWindow() {
    WindowPosTask task;
    sdlThread->addTask(&task);
    task.wait();

    if (task.getX() == 0 && task.getY() == 0)
        return;

    int newX = task.getX();
    int newY = task.getY() + task.getHeight() + 8;
    if (pos().x() != newX || pos().y() != newY)
        move(newX, newY);
}

void DeveloperWindow::onSnapTimerTimeout() {
    snapToSDLWindow();
}

void DeveloperWindow::on_loadWorldButton_clicked() {
    QString filePath = QFileDialog::getOpenFileName(this, "Open World File", QString(), "World JSON (*.json)");
    if (filePath.isNull() == false) {
        QFileInfo fileInfo(filePath);
        QString libraryPath = fileInfo.absoluteDir().absolutePath();

        LoadWorldTask *task = new LoadWorldTask(sdlThread, filePath, libraryPath);
        sdlThread->addTask(task);
    }
}

void DeveloperWindow::on_globalLoopLockCheckBox_toggled(bool checked) {
    SetGlobalLoopLockTask task(sdlThread, checked);
    sdlThread->addTask(&task);
    qDebug() << "DeveloperWindow added SetGlobalLoopLockTask";
    task.wait();
}

void DeveloperWindow::on_setPosButton_clicked() {
    bool ok;
    unsigned newX = ui->setPosXText->text().toUInt(&ok);
    if (!ok) { return; }
    unsigned newY = ui->setPosYText->text().toUInt(&ok);
    if (!ok) { return; }

    WindowPosTask task(newX, newY);
    sdlThread->addTask(&task);
    task.wait();
}

void DeveloperWindow::on_snapButton_clicked() {
    snapToSDLWindow();
}
