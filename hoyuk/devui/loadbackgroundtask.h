#ifndef LOADBACKGROUNDTASK_H
#define LOADBACKGROUNDTASK_H

#include "../sdlthread.h"
#include "../resourceloader.h"

class LoadBackgroundTask : public ExtTask {
private:
    SDLThread *sdlThread;
    QString filePath;

public:
    LoadBackgroundTask(SDLThread *sdlThread, const QString &filePath) : ExtTask("LoadBackgroundTask") {
        this->sdlThread = sdlThread;
        this->filePath = filePath;
    }

    void run() {
        Background *background = loadBackgroundResource(filePath);

        if (background) {
            qDebug() << "LoadBackgroundTask adding background to runtime";
            sdlThread->addBackground(background);
        }
    }
};

#endif // LOADBACKGROUNDTASK_H
