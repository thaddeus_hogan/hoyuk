#include "devui\developerdock.h"
#include "ui_developerdock.h"

DeveloperDock::DeveloperDock(SDLThread *sdlThread) :  QMainWindow(NULL), ui(new Ui::DeveloperDock) {
    ui->setupUi(this);
    this->sdlThread = sdlThread;
    setWindowFlags(Qt::FramelessWindowHint);

    connect(&initTimer, SIGNAL(timeout()), this, SLOT(initTimerTimeout()));
    connect(ui->titleLabel, SIGNAL(titleLabelDragged(QPoint)), this, SLOT(onTitleLabelDragged(QPoint)));

    initTimer.setSingleShot(true);
    initTimer.start(0);
}

DeveloperDock::~DeveloperDock() {
    delete ui;
}

void DeveloperDock::initTimerTimeout() {
    // Try to do some reasonable initial positioning of the game window
    int newX;
    int newY;
    int newSdlY;

    QSize dispSize = sdlThread->getDisplaySize();
    QSize screenSize = QApplication::desktop()->availableGeometry(this).size();
    resize(dispSize.width(), size().height());
    newX = (screenSize.width() / 2) - (dispSize.width() / 2);
    newSdlY = (screenSize.height() / 2) - (dispSize.height() / 2);
    newY = newSdlY - size().height();
    move(newX, newY);
    setSDLWindowPos(QPoint(newX, newSdlY));
}

void DeveloperDock::onTitleLabelDragged(QPoint newPos) {
    move(newPos);
    QPoint newSdlPos(newPos);
    newSdlPos.setY(pos().y() + size().height());
    setSDLWindowPos(newSdlPos);
}

void DeveloperDock::setSDLWindowPos(QPoint newPos) {
    WindowPosTask task(newPos.x(), newPos.y());
    sdlThread->addTask(&task);
    task.wait();
}

void DeveloperDock::on_exitButton_clicked() {
    sdlThread->shutdown();
}

void DeveloperDock::on_loadWorldButton_clicked() {
    QString worldFilePath = QFileDialog::getOpenFileName(this, "Load Hoyuk World File", QString(), "World Files (*.json)");

    if (worldFilePath.isNull() == false) {
        QString libraryPath = QFileInfo(worldFilePath).absoluteDir().absolutePath();
        LoadWorldTask *task = new LoadWorldTask(sdlThread, worldFilePath, libraryPath);
        sdlThread->addTask(task);
    }
}
