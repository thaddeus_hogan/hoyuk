#ifndef RESOURCEENTRY_H
#define RESOURCEENTRY_H

#include <archive.h>
#include <QtCore>

class ResourceEntry {
protected:
    QString _name;
    qint64 _size;

public:
    ResourceEntry(const QString &name, qint64 size);

    const QString& name() const;
    qint64 size() const;

    virtual long read(char *buf, unsigned count) = 0;
};

class ArchiveResourceEntry : public ResourceEntry {
protected:
    archive *_ar;

public:
    ArchiveResourceEntry(archive *ar ,const QString &name, qint64 size);
    long read(char *buf, unsigned count);
};

class FileResourceEntry : public ResourceEntry {
protected:
    QFile *_file;

public:
    FileResourceEntry(const QString &name, QFile *file);
    ~FileResourceEntry();
    long read(char *buf, unsigned count);
};

#endif // RESOURCEENTRY_H
