#include "backgroundreader.h"

BackgroundReader::BackgroundReader() {
    this->background = NULL;
}

Background* BackgroundReader::getBackground() {
    return background;
}

void BackgroundReader::handleEntry(ResourceEntry &re) {
    if (re.name().compare("background.png", Qt::CaseInsensitive) == 0) {
        bgImg.pngDataSize = re.size();
        bgImg.pngData = new unsigned char[bgImg.pngDataSize];

        if (re.read((char*)bgImg.pngData, bgImg.pngDataSize) <= 0) {
            loadResult.errorCode = IMAGE_EXTRACT_FAILED;
            loadResult.errorMessage = "Extracting PNG data from ZIP file failed";
        }
    }
}

void BackgroundReader::postRead() {
    bool ok; // Used for number parse results

    QString resType = meta["type"].toString();

    // Make sure this resource archive is a Background
    if (resType.compare("background", Qt::CaseInsensitive) != 0) {
        loadResult.errorCode = BAD_META_DATA;
        loadResult.errorMessage =
            QString("Asked to load background resource, but archive meta is for ")
            .append(resType);
        return;
    }

    //
    // Process and error-check meta data
    //
    QString name = meta["name"].toString();
    if (name.isNull()) { setErrorMissing("name"); }

    double width = meta["width"].toDouble(&ok);
    if (!ok) { setErrorMissing("width"); }
    double height = meta["height"].toDouble(&ok);
    if (!ok) { setErrorMissing("height"); }

    unsigned texRes = meta["resolution"].toUInt(&ok);
    if (!ok) { setErrorMissing("resolution"); }

    unsigned renderTop = meta["renderTop"].toUInt(&ok);
    if (!ok) { setErrorMissing("renderTop"); }
    unsigned renderLeft = meta["renderLeft"].toUInt(&ok);
    if (!ok) { setErrorMissing("renderLeft"); }
    unsigned renderBottom = meta["renderBottom"].toUInt(&ok);
    if (!ok) { setErrorMissing("renderBottom"); }
    unsigned renderRight = meta["renderRight"].toUInt(&ok);
    if (!ok) { setErrorMissing("renderRight"); }

    if (loadResult.errorCode != OK) {
        return;
    }

    // Create texture rect
    TextureRect trect(texRes, renderTop, renderLeft, renderBottom, renderRight);

    // Process image data and create texture
    FIBITMAP *bitmap = loadBitmap(bgImg);

    if (!bitmap) {
        loadResult.errorCode = IMAGE_EXTRACT_FAILED;
        loadResult.errorMessage = "BackgroundReader Failed to decompress PNG data for background";
        return;
    }

    // Create texture on GPU for this image
    qDebug() << "BackgroundReader creating OpenGL texture background " << name;

    GLuint newTexID;
    createTexture(&newTexID, bitmap, texRes, loadResult);
    FreeImage_Unload(bitmap);

    if (loadResult.errorCode != OK) {
        return;
    }

    background = new Background(name, width, height, newTexID, trect);
}

void BackgroundReader::clean() {
    delete bgImg.pngData;
    bgImg.pngData = NULL;
    bgImg.pngDataSize = 0;
}
