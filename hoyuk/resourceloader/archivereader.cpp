#include "resourceloader\archivereader.h"

ArchiveReader::ArchiveReader() {
    this->ar = NULL;
    this->entry = NULL;
}

void ArchiveReader::readArchive(const QString& filePath) {
    QDir fileDir(filePath);
    if (fileDir.exists()) {
        //
        // Handle directory resource archive
        //
        qDebug() << "ArchiveReader handling resource directory";

        QStringList entries = getAllEntries(fileDir, QString());
        qDebug() << "ArchiveReader read " << entries.size() << " entries from directory " << fileDir.absolutePath();

        foreach (QString entryName, entries) {
            QFile entryFile(fileDir.absoluteFilePath(entryName));
            if (entryFile.exists() == false) {
                loadResult.errorCode = FILE_NOT_FOUND;
                loadResult.errorMessage = QString("ArchiveReader Failed to find entry in resource directory: ")
                    .append(QFileInfo(entryFile).absolutePath());
                qDebug() << loadResult.errorMessage;
                return;
            }

            FileResourceEntry re(entryName, &entryFile);

            // -------- Handle meta
            if (re.name().compare("meta.json", Qt::CaseInsensitive) == 0) {
                handleMeta(re);

                if (loadResult.errorCode == OK) {
                    qDebug() << "ArchiveReader parsed meta.json from " << filePath;
                } else {
                    qDebug() << "ArchiveReader failed to parse meta.json from "
                             << filePath << " with error: " << loadResult.errorMessage;
                    break;
                }
            } else {
                handleEntry(re);
            }
        }

    } else if (QFileInfo(QString(filePath).append(".zip")).exists()) {
        //
        // Handle ZIP file resource archive
        //
        qDebug() << "ArchiveReader handling resource ZIP file";

        int arOpenResult;

        ar = archive_read_new();
        archive_read_support_filter_all(ar);
        archive_read_support_format_zip(ar);

        qDebug() << "ArchiveReader opening archive " << filePath;
        arOpenResult = archive_read_open_filename_w(ar, filePath.utf16(), 32768);
        qDebug() << "ArchiveReader archive opened " << filePath;

        if (arOpenResult != ARCHIVE_OK) {
            loadResult.errorCode = ARCHIVE_OPEN_FAILED;
            loadResult.errorMessage = "Archive open failed, archive is corrupt or invalid";
            return;
        }

        while (archive_read_next_header(ar, &entry) == ARCHIVE_OK) {
            ArchiveResourceEntry re(ar, archive_entry_pathname(entry), archive_entry_size(entry));

            // -------- Handle meta
            if (re.name().compare("meta.json", Qt::CaseInsensitive) == 0) {
                handleMeta(re);

                if (loadResult.errorCode == OK) {
                    qDebug() << "ArchiveReader parsed meta.json from " << filePath;
                } else {
                    qDebug() << "ArchiveReader failed to parse meta.json from "
                             << filePath << " with error: " << loadResult.errorMessage;
                    break;
                }
            } else {
                handleEntry(re);
            }
        }

        qDebug() << "ArchiveReader finished reading archive " << filePath;
        // Close archive, begin processing data from archive
        archive_read_free(ar);
        qDebug() << "ArchiveReader freed archive " << filePath;
    } else {
        //
        // filePath not a directory and dosen't end in .zip
        //
        loadResult.errorCode = FILE_NOT_FOUND;
        loadResult.errorMessage = QString(
            "ArchiveReader can open directories or ZIP files (must end in .zip). Failed on: ")
            .append(filePath);
        qDebug() << loadResult.errorMessage;
    }

    // If archive read failed, skip post-processing, but always run clean() before returning
    if (loadResult.errorCode != OK) {
        qDebug() << "ArchiveReader error reading archive entries: " << loadResult.errorMessage;
        clean();
        return;
    }

    // Process data that was found in archive
    qDebug() << "ArchiveReader finished reading " << filePath << " - starting post processing";
    postRead();

    clean();
}

void ArchiveReader::handleMeta(ResourceEntry &re) {
    if (re.size() > 1048576) {
        loadResult.errorCode = BAD_META_DATA;
        loadResult.errorMessage = "meta.json > 1MB in size, refusing to process";
        return;
    }

    // TODO - Since there may be many resources, it may be more performant
    // to just alloc a large buffer and re-use it for every resource load
    char *metaBuf = new char[re.size() + 1];

    if (re.read(metaBuf, re.size()) > 0) {
        qDebug() << "ArchiveReader read data for meta.json";

        metaBuf[re.size()] = '\0';
        bool parseResult = false;

        // JSON IS PARSED HERE
        meta = jsonParser.parse(QByteArray(metaBuf), &parseResult).toMap();

        if (parseResult == false) {
            loadResult.errorCode = JSON_PARSE_FAILED;
            loadResult.errorMessage = "QJson parse of meta.json failed";
        }
    } else {
        loadResult.errorCode = BAD_META_DATA;
        loadResult.errorMessage = "Failed to read meta.json from ZIP file";
    }

    delete metaBuf;
}

const LoadResult& ArchiveReader::getLoadResult() const {
    return loadResult;
}

void ArchiveReader::setErrorMissing(const QString & missing) {
    loadResult.errorCode = BAD_META_DATA;
    loadResult.errorMessage = QString("Missing meta data field: ").append(missing);
}

QStringList ArchiveReader::getAllEntries(const QDir &dir, const QString &prefix) {
    QStringList entries;
    QDir thisDir(dir.absoluteFilePath(prefix));

    if (thisDir.exists() == false) {
        loadResult.errorCode = FILE_NOT_FOUND;
        loadResult.errorMessage = QString("ArchiveReader getAllEntries() recursed into non-existent directory: ")
            .append(thisDir.absolutePath());
        qDebug() << loadResult.errorMessage;
        return entries;
    }

    foreach (QString entry, thisDir.entryList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot)) {
        QFileInfo entryInfo(thisDir.absoluteFilePath(entry));
        if (entryInfo.isDir()) {
            entries.append(getAllEntries(dir, QString(prefix).append(entry).append("/")));
        } else {
            entries.append(QString(prefix).append(entry));
        }
    }

    return entries;
}
