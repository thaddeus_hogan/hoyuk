#ifndef IMAGETOOLS_H
#define IMAGETOOLS_H

#include <QDebug>
#include <GL/glew.h>
#include <FreeImage.h>

#include "loadresult.h"
#include "imageresource.h"

FIBITMAP* loadBitmap(ImageResource &ir);
void createTexture(GLuint *texID, FIBITMAP* bitmap, unsigned texRes, LoadResult &loadResult);

namespace ImageTools {
    unsigned DLL_CALLCONV lrImageRead(void* buffer, unsigned size, unsigned count, fi_handle handle);
    int DLL_CALLCONV lrImageSeek(fi_handle handle, long offset, int origin);
    long DLL_CALLCONV lrImageTell(fi_handle handle);
}

#endif // IMAGETOOLS_H
