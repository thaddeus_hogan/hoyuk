#ifndef FRAMERESOURCE_H
#define FRAMERESOURCE_H

#include "imageresource.h"

class FrameResource : public ImageResource {
public:
    unsigned frameNum;

    FrameResource() {
        this->frameNum = -1;
    }

    FrameResource(const FrameResource &other) : ImageResource(other) {
        this->frameNum = other.frameNum;
    }
};

#endif // FRAMERESOURCE_H
