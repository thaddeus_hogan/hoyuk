#ifndef ARCHIVEREADER_H
#define ARCHIVEREADER_H

#include <archive.h>
#include <archive_entry.h>
#include <parser.h>

#include "loadresult.h"
#include "resourceentry.h"

class ArchiveReader {
protected:
    archive *ar;
    archive_entry *entry;

    QJson::Parser jsonParser;
    QVariantMap meta;
    LoadResult loadResult;

    void handleMeta(ResourceEntry &re);
    void setErrorMissing(const QString & missing);
    QStringList getAllEntries(const QDir &dir, const QString &prefix);

    virtual void handleEntry(ResourceEntry &re) = 0;
    virtual void postRead() = 0;
    virtual void clean() = 0;

public:
    ArchiveReader();

    void readArchive(const QString& filePath);
    const LoadResult& getLoadResult() const;
};

#endif // ARCHIVEREADER_H
