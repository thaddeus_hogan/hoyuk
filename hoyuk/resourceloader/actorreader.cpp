#include "actorreader.h"

unsigned ActorReader::instCount = 0;

ActorReader::ActorReader() {
    this->actor = NULL;
    rxTexFileName = QRegExp("textures/(\\w+)/(\\d{3})\\.png", Qt::CaseInsensitive, QRegExp::RegExp2);
}

Actor* ActorReader::getActor() {
    return actor;
}

void ActorReader::readArchive(const QString& filePath) {
    instCount += 1; // Instance counter

    scriptModule = NULL;
    frameResources.clear();
    frameBounds.clear();
    states.clear();
    metaStates = QVariantMap();

    ArchiveReader::readArchive(filePath);
}

void ActorReader::handleEntry(ResourceEntry &re) {
    if (rxTexFileName.exactMatch(re.name()) == true) {
        handleFrameEntry(rxTexFileName.cap(1), rxTexFileName.cap(2).toUInt(), re);
    } else if (re.name().compare("actor.as", Qt::CaseInsensitive) == 0) {
        handleScriptEntry(re);
    }
}

void ActorReader::handleFrameEntry(const QString &state, unsigned frameNum, ResourceEntry &re) {
    FrameResource frame;
    frame.frameNum = frameNum;
    frame.pngDataSize = re.size();
    frame.pngData = new unsigned char[frame.pngDataSize];

    if (re.read((char*)frame.pngData, frame.pngDataSize) > 0) {
        frameResources[state].append(frame);

        FrameBounds &bounds = frameBounds[state];
        if (frame.frameNum < bounds.lowest)
            bounds.lowest = frame.frameNum;
        if (frame.frameNum > bounds.highest)
            bounds.highest = frame.frameNum;

        if (states.contains(state) == false)
            states << state;
    } else {
        loadResult.errorCode = IMAGE_EXTRACT_FAILED;
        loadResult.errorMessage = "Extracting PNG data from ZIP file failed";
    }
}

void ActorReader::handleScriptEntry(ResourceEntry &re) {
    char *scriptSrcBuf = new char[re.size() + 1];
    re.read(scriptSrcBuf, re.size());
    scriptSrcBuf[re.size()] = '\0';

    QString moduleName("Actor");
    moduleName.append(QString().setNum(ActorReader::instCount));

    qDebug() << "ActorReader Creating script module: " << moduleName;
    if (ScriptUtil::instance().buildScriptModule(moduleName, scriptSrcBuf) == false) {
        qDebug() << "ActorReader build module failed for module: " << moduleName;
    }

    scriptModule = ScriptUtil::instance().getModule(moduleName);
}

void ActorReader::postRead() {
    //
    // Start parsing the meta data and create the actor resource
    //
    QString resType = meta["type"].toString();

    // Check that the meta says this resource is an actor
    if (resType.compare("actor", Qt::CaseInsensitive) != 0) {
        loadResult.errorCode = BAD_META_DATA;
        loadResult.errorMessage = QString("Asked to load actor resource, but archive meta is for ").append(resType);
        return;
    }

    QString actorName = meta["name"].toString();
    if (actorName.isNull()) { setErrorMissing("name"); }

    actor = new Actor(actorName);
    actor->setScriptModule(scriptModule);

    bool ok = false;

    double width = meta["width"].toDouble(&ok);
    if (!ok) { setErrorMissing("width"); }
    double height = meta["height"].toDouble(&ok);
    if (!ok) { setErrorMissing("height"); }

    actor->setDimensions(width, height);

    QString defaultState = meta["defaultState"].toString();
    if (defaultState.isNull()) { setErrorMissing("defaultState"); }
    actor->setDefaultState(defaultState);

    unsigned texRes = meta["resolution"].toUInt(&ok);
    if (!ok) { setErrorMissing("resolution"); }

    if (meta.contains("directionalX"))
        actor->setDirectionalX(meta["directionalX"].toBool());

    if (meta.contains("directionalY"))
        actor->setDirectionalY(meta["directionalY"].toBool());

    metaStates = meta["states"].toMap();
    if (metaStates.isEmpty()) { setErrorMissing("states"); }

    // If there were errors reading any of the essentials, bail here before
    // getting into processing the animation frames
    if (loadResult.errorCode != OK) {
        return;
    }

    //
    // Process 2D geometry
    //
    QVariantMap geometry = meta["geometry"].toMap();
    if (geometry.isEmpty()) {
        setErrorMissing("geometry");
        return;
    }

    b2BodyDef bodyDef;
    if (geometry.contains("fixedRotation")) {
        bodyDef.fixedRotation = geometry["fixedRotation"].toBool();
    } else {
        bodyDef.fixedRotation = false;
    }

    bodyDef.type = b2_dynamicBody;
    actor->setBodyDef(bodyDef);

    // Process fixtures
    if (geometry.contains("fixtures") == false) {
        setErrorMissing("geometry -> fixtures");
        return;
    }

    QVariantList fixtures = geometry["fixtures"].toList();
    foreach (QVariant fixtureObj, fixtures) {
        b2FixtureDef fixtureDef;

        QVariantMap fixture = fixtureObj.toMap();
        if (fixture.contains("vertices") == false)
            continue;

        // Process vertices within a fixture
        b2PolygonShape *shape = new b2PolygonShape();
        QVariantList vertices = fixture["vertices"].toList();
        int vcount = vertices.size();
        b2Vec2 *vlist = new b2Vec2[vcount];

        for (int vidx = 0; vidx < vcount; vidx++) {
            QVariantList vpair = vertices.at(vidx).toList();
            vlist[vidx].x = vpair.at(0).toFloat();
            vlist[vidx].y = vpair.at(1).toFloat();
            qDebug() << "ActorReader added shape vertex X:" << vlist[vidx].x << " Y:" << vlist[vidx].y;
        }

        shape->Set(vlist, vcount);
        fixtureDef.shape = shape;

        fixtureDef.density = fixture["density"].toFloat(&ok);
        if (!ok) { setErrorMissing("geometry -> fixtures -> density"); }
        fixtureDef.restitution = fixture["restitution"].toFloat(&ok);
        if (!ok) { setErrorMissing("geometry -> fixtures -> restitution"); }
        fixtureDef.friction = fixture["friction"].toFloat(&ok);
        if (!ok) { setErrorMissing("geometry -> fixtures -> friction"); }

        actor->getFixtures().append(fixtureDef);

        delete vlist;
    }

    if (loadResult.errorCode != OK)
        return;

    //
    // Process animation frames
    //

    // FrameResource iterator used below
    QList<FrameResource>::iterator frameIt;

    // Prepare the animation states by decompressing the PNG data and
    // uploading the frame textures to the GPU
    foreach (QString state, states) {
        qDebug() << "ActorReader processing data for animation state " << state;

        if (metaStates.contains(state) == false) {
            loadResult.errorCode = BAD_META_DATA;
            loadResult.errorMessage = QString("meta.json missing data for state: ").append(state);
            break;
        }

        // Gather metadata
        QVariantMap stateData = metaStates[state].toMap();
        FrameBounds bounds = frameBounds.value(state);

        if (bounds.lowest != 0) {
            loadResult.errorCode = BAD_META_DATA;
            loadResult.errorMessage = QString("Frame count does not start at 0 for state ").append(state);
            break;
        }

        unsigned numFrames = bounds.highest + 1;

        unsigned renderTop = stateData["renderTop"].toUInt();
        unsigned renderBottom = stateData["renderBottom"].toUInt();
        unsigned renderLeft = stateData["renderLeft"].toUInt();
        unsigned renderRight = stateData["renderRight"].toUInt();
        unsigned frameRate = stateData["frameRate"].toUInt();

        TextureRect trect(texRes, renderTop, renderLeft, renderBottom, renderRight);
        AnimationState animState(numFrames, trect, frameRate);

        // Process image data and create textures
        for (frameIt = frameResources[state].begin(); frameIt != frameResources[state].end(); ++frameIt) {
            FrameResource &frame = *frameIt;

            qDebug() << "ActorReader decompressing PNG data for frame "
                     << frame.frameNum << " of state " << state;

            FIBITMAP *bitmap = loadBitmap(frame);

            if (!bitmap) {
                loadResult.errorCode = IMAGE_EXTRACT_FAILED;
                loadResult.errorMessage = QString("Failed to decompress PNG data for frame ")
                    .append(QString().setNum(frame.frameNum))
                    .append(" of state ").append(state);
                break;
            }

            // Release PNG data
            delete frame.pngData;
            frame.pngData = NULL;

            // Create texture on GPU for this image
            qDebug() << "ActorReader creating OpenGL texture for frame "
                     << frame.frameNum << " of state " << state;

            GLuint newTexID;
            createTexture(&newTexID, bitmap, texRes, loadResult);
            FreeImage_Unload(bitmap);

            if (loadResult.errorCode != OK) {
                loadResult.errorMessage.append(" - frame ")
                    .append(QString().setNum(frame.frameNum))
                    .append(" of state ").append(state);
            }

            animState.setFrameTexID(frame.frameNum, newTexID);

            qDebug() << "ActorReader uploaded to GPU frame "
                     << frame.frameNum << " of state " << state;
        }

        // Stop processing if a frame load failed
        if (loadResult.errorCode != OK)
            break;

        actor->setAnimationState(state, animState);
    }
}

void ActorReader::clean() {
    // Run through frame resources and make sure any heap data is freed
    QList<FrameResource>::iterator frameIt;

    foreach (QString state, states) {
        for (frameIt = frameResources[state].begin(); frameIt != frameResources[state].end(); ++frameIt) {
            FrameResource frame = *frameIt;
            if (frame.pngData != NULL) {
                delete frame.pngData;
                frame.pngData = NULL;

                qDebug() << "LoadResource freed un-reaped PNG data for frame "
                         << frame.frameNum << " of state " << state;
            }
        }
    }
}
