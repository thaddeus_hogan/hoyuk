#ifndef SCRIPTQSTRING_H
#define SCRIPTQSTRING_H

#include <QString>
#include <assert.h>
#include "angelscript.h"

void RegisterQString(asIScriptEngine *engine);

#endif // SCRIPTQSTRING_H
