#include "scriptqstring.h"

static QString StringFactory(asUINT length, const char *s) {
    return QString::fromUtf8(s, length);
}

static void ConstructString(QString *thisPointer) {
    new(thisPointer) QString();
}

static void CopyConstructString(const QString &other, QString *thisPointer) {
    new(thisPointer) QString(other);
}

static void DestructString(QString *thisPointer) {
    thisPointer->~QString();
}

static QString StringAddString(const QString &lhs, const QString &rhs) {
    return QString(lhs).append(rhs);
}

static bool StringEquals(const QString &lhs, const QString &rhs) {
    return (lhs.compare(rhs) == 0);
}

static QString StringAddInt(const QString &lhs, int i) {
    QString numStr = QString().setNum(i);
    return QString(lhs).append(numStr);
}

static QString StringAddFloat(const QString &lhs, float i) {
    QString numStr = QString().setNum(i);
    return QString(lhs).append(numStr);
}

static QString StringAddDouble(const QString &lhs, double i) {
    QString numStr = QString().setNum(i);
    return QString(lhs).append(numStr);
}

void RegisterQString(asIScriptEngine *engine) {
    int r;
    r = engine->RegisterObjectType("string", sizeof(QString), asOBJ_VALUE | asOBJ_APP_CLASS_CDAK); assert(r >= 0);
    r = engine->RegisterStringFactory("string", asFUNCTION(StringFactory), asCALL_CDECL); assert(r >= 0);
    r = engine->RegisterObjectBehaviour("string", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(ConstructString), asCALL_CDECL_OBJLAST); assert(r >= 0);
    r = engine->RegisterObjectBehaviour("string", asBEHAVE_CONSTRUCT, "void f(const string &in)", asFUNCTION(CopyConstructString), asCALL_CDECL_OBJLAST);assert (r >= 0);
    r = engine->RegisterObjectBehaviour("string", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(DestructString), asCALL_CDECL_OBJLAST); assert(r >= 0);
    r = engine->RegisterObjectMethod("string", "bool opEquals(const string &in) const", asFUNCTION(StringEquals), asCALL_CDECL_OBJFIRST); assert(r >= 0);
    r = engine->RegisterObjectMethod("string", "string &opAssign(const string &in)", asMETHODPR(QString, operator =, (const QString&), QString&), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("string", "string opAdd(const string &in) const", asFUNCTION(StringAddString), asCALL_CDECL_OBJFIRST); assert(r >= 0);
    r = engine->RegisterObjectMethod("string", "string &opAddAssign(const string &in)", asMETHODPR(QString, append, (const QString&), QString&), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("string", "string opAdd(int) const", asFUNCTION(StringAddInt), asCALL_CDECL_OBJFIRST); assert(r >= 0);
    r = engine->RegisterObjectMethod("string", "string opAdd(float) const", asFUNCTION(StringAddFloat), asCALL_CDECL_OBJFIRST); assert(r >= 0);
    r = engine->RegisterObjectMethod("string", "string opAdd(double) const", asFUNCTION(StringAddDouble), asCALL_CDECL_OBJFIRST); assert(r >= 0);
}
