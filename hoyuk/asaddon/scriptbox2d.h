#ifndef SCRIPTBOX2D_H
#define SCRIPTBOX2D_H

#include <assert.h>
#include <angelscript.h>
#include <Box2D/Box2D.h>

void RegisterBox2D(asIScriptEngine *engine);

#endif // SCRIPTBOX2D_H
