#ifndef SCRIPTUTIL_H
#define SCRIPTUTIL_H

#include <QtCore>
#include <angelscript.h>
#include "asaddon/scriptbuilder.h"

class ScriptUtil {
private:
    ScriptUtil();
    asIScriptEngine *scr;
    asIScriptContext *_scrCtx;

public:
    static ScriptUtil& instance() {
        static ScriptUtil inst;
        return inst;
    }

    void setScriptEngine(asIScriptEngine *scr);
    bool buildScriptModule(const QString &moduleName, const char *src);
    bool buildScriptModule(const QString &moduleName, const QStringList &fileList);
    asIScriptModule* getModule(const char *moduleName);
    asIScriptModule* getModule(const QString &moduleName);

    asIScriptContext* scrCtx();
    void scrCtx(asIScriptContext *_scrCtx);
};

#endif // SCRIPTUTIL_H
