#ifndef WORLDLOADER_H
#define WORLDLOADER_H

#include <QtCore>
#include <parser.h>
#include "scriptutil.h"
#include "resourceloader.h"
#include "sdlthread.h"
#include "logic/cameralogic.h"
#include "logic/followxlogic.h"

class WorldLoader {
protected:
    LoadResult loadResult;
    QFile worldFile;
    QDir libraryDir;
    QVariantList worldData;
    SDLThread *sdlThread;
    unsigned instID; // Increment to give unique ID to each ActorInstance

    void handleWorld(QVariantMap &data);
    void handleBackground(QString resource, QVariantMap &data);
    void handleBackgroundRepeat(QString resource, QVariantMap &data);
    void handleActor(QString resource, QVariantMap &data);
    void handleChain(QVariantMap &data);

    Background* backgroundAvailable(QString name);
    Actor* actorAvailable(QString name);

public:
    bool fullReload;

    WorldLoader(SDLThread *sdlThread, const QString &filePath, const QString &libraryPath);
    LoadResult loadWorld(bool fullReload = false);
};

class LoadWorldTask : public ExtTask {
protected:
    SDLThread *sdlThread;
    QString filePath;
    QString libraryPath;
public:
    LoadWorldTask(SDLThread *sdlThread, const QString& filePath, const QString &libraryPath);
    void run();
};

#endif // WORLDLOADER_H
