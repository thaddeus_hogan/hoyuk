#include <Windows.h>
#include "hoyuktime.h"

quint64 HoyukTime::nanosPerTick = 0;
quint64 HoyukTime::lastTick = 0;

HoyukTime::HoyukTime() {
}

void HoyukTime::initTime() {
    // Set the current thread's processor affinity to any
    // processor within the current process' affinity mask
    unsigned bits = sizeof(DWORD_PTR) * 8;
    DWORD_PTR procMask;
    DWORD_PTR sysMask;
    DWORD_PTR threadMask;

    GetProcessAffinityMask(GetCurrentProcess(), &procMask, &sysMask);
    for (unsigned i = 0; i < bits; i++) {
        threadMask = procMask & (1LL<<i);
        if (threadMask > 0)
            break;
    }

    SetThreadAffinityMask(GetCurrentThread(), threadMask);

    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    assert(freq.QuadPart > 0);

    HoyukTime::nanosPerTick = 1000000000LL / freq.QuadPart;
    HoyukTime::reset();
}

quint64 HoyukTime::nanosElapsed() {
    LARGE_INTEGER count;
    QueryPerformanceCounter(&count);
    return (count.QuadPart - HoyukTime::lastTick) * nanosPerTick;
}

void HoyukTime::reset() {
    LARGE_INTEGER count;
    QueryPerformanceCounter(&count);
    HoyukTime::lastTick = count.QuadPart;
}
