#ifndef HOYUKTIMEWIN32_H
#define HOYUKTIMEWIN32_H

#include <assert.h>
#include <QtGlobal>

class HoyukTime {
private:
    HoyukTime();
public:
    static quint64 nanosPerTick;
    static quint64 lastTick;

    static void initTime();
    static quint64 nanosElapsed();
    static void reset();
};

#endif // HOYUKTIMEWIN32_H
