#pragma once

#include <stdio.h>
#include <cmath>
#include <Windows.h>

#include <SDL.h>
#include <GL\glew.h>
#include <FreeImage.h>
#include <Box2D\Box2D.h>

#define MIN_FRAME_TIME 16
#define DOG_MAX_VEL 4

class App {
private:
	bool doRun;
	SDL_Surface *mainSurface;
	SDL_Joystick *joyA;

	Sint16 joyX;
	Sint16 joyY;

	GLdouble sceneW;
	GLdouble sceneH;
	GLdouble sceneXScale;
	GLdouble sceneYScale;

	GLdouble boxY;
	GLdouble boxX;
	GLdouble boxDir;
	GLdouble boxVel;
	GLdouble boxAngle;

	GLuint dogTexID;

	float32 phyTimeStep;
	b2World *world;
	b2Body *bodyGround;
	b2Body *bodyDog;

	Uint32 lastTick;
	Uint32 ms250c;

	bool init();
	void onEvent(SDL_Event* ev);
	void onLoop();
	void render();
	void destroy();

public:
	App(void);
	~App(void);

	int execute(void);
	void stop();
};

