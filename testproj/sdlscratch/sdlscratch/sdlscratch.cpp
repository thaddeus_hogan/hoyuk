// sdlscratch.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "App.h"
#include <SDL.h>

int main(int argc, char* argv[]) {
	App *app = new App();
	int retVal = app->execute();
	delete app;
	return retVal;
}

