#include "StdAfx.h"
#include "App.h"

App::App(void) {
	this->doRun = true;

	this->joyX = 0;
	this->joyY = 0;

	this->world = NULL;
	this->phyTimeStep = 1.0f / 62.5f;

	this->sceneW = 6; // meters
	this->sceneH = 4.5; // meters
	this->sceneXScale = 1024 / sceneW; // pixels per meter
	this->sceneYScale = 768 / sceneH; // pixels per meter

	this->boxX = 1;
	this->boxY = 2;
	this->boxDir = 1;
	this->boxVel = 0.001; // meters per milli
	this->boxAngle = 0;
}

App::~App(void) {
}

int App::execute() {
	if (init() == false) {
		return 1;
	}

	SDL_Event ev;

	while (doRun == true) {
		while (SDL_PollEvent(&ev)) {
			onEvent(&ev);
		}

		onLoop();
		render();

		Uint32 waitForTick = lastTick + MIN_FRAME_TIME;
		while (SDL_GetTicks() < waitForTick) {
			SDL_Delay(1);
		}
	}

	destroy();

	return 0;
}

void App::stop() {
	doRun = false;
}

bool App::init() {
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		return false;
	}

	// Setup joystick input
	SDL_JoystickEventState(SDL_ENABLE);
	joyA = SDL_JoystickOpen(0);

	// Setup OpenGL Parameters
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE,         8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,       8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,        8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,       8);
 
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,       16);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,      32);
 
	SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE,   8);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE,  8);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE, 8);
 
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,  1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,  2);

	mainSurface = SDL_SetVideoMode(1024, 768, 32, SDL_HWSURFACE | SDL_GL_DOUBLEBUFFER | SDL_OPENGL);
	if (mainSurface == NULL) {
		return false;
	}

	// Init GL view area
	glClearColor(0, 0, 0, 1);
	glViewport(0, 0, 1024, 768);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 1024, 768, 0, 1, -1);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glLoadIdentity();

	GLenum glError = glGetError();
	if (glError != GL_NO_ERROR) {
		OutputDebugString(_T("GL Initialization Error:"));
		//OutputDebugStringA((LPCSTR)gluErrorString(glError));
		return false;
	}

	// Load dog texture
	char dogFile[] = "C:\\dev\\gamews\\sdlscratch\\img\\sheltie256.png";
	FIBITMAP *bmap;
	BYTE* imgData;

	FREE_IMAGE_FORMAT fif = FIF_PNG;
	bmap = FreeImage_Load(FIF_PNG, dogFile);
	if (!bmap) {
		return false;
	}

	imgData = FreeImage_GetBits(bmap);

	glGenTextures(1, &dogTexID);
	glBindTexture(GL_TEXTURE_2D, dogTexID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 256, 256, 0, GL_BGRA, GL_UNSIGNED_BYTE, imgData);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, NULL);

	FreeImage_Unload(bmap);

	// On to PHYSICS!
	b2Vec2 grav(0, 8);
	world = new b2World(grav);

	// Da ground
	b2BodyDef bodyGroundDef;
	bodyGroundDef.position.Set(3, 4.25);
	bodyGround = world->CreateBody(&bodyGroundDef);
	b2PolygonShape bodyGroundShape;
	bodyGroundShape.SetAsBox(3, 0.25);
	bodyGround->CreateFixture(&bodyGroundShape, 0);

	// Da dog
	b2BodyDef bodyDogDef;
	bodyDogDef.fixedRotation = true;
	bodyDogDef.type = b2_dynamicBody;
	bodyDogDef.position.Set(3, 1);
	bodyDog = world->CreateBody(&bodyDogDef);

	b2PolygonShape bodyDogShape;
	bodyDogShape.SetAsBox(0.5, 0.5);
	b2FixtureDef bodyDogFixtureDef;
	bodyDogFixtureDef.shape = &bodyDogShape;
	bodyDogFixtureDef.density = 16;
	bodyDogFixtureDef.restitution = 0.2;
	bodyDogFixtureDef.friction = 0.9;

	bodyDog->CreateFixture(&bodyDogFixtureDef);

	// Get the ball rolling
	lastTick = SDL_GetTicks();
	ms250c = 0;

	return true;
}

void App::onEvent(SDL_Event* ev) {
	if (ev->type == SDL_QUIT) {
		stop();
	} else if (ev->type == SDL_JOYAXISMOTION) {
		if (ev->jaxis.axis == 0) {
			joyX = ev->jaxis.value;
		} else if (ev->jaxis.axis == 1) {
			joyY = ev->jaxis.value;
		}
	}
}

void App::onLoop() {
	Uint32 ticksElapsed = SDL_GetTicks() - lastTick;
	lastTick = SDL_GetTicks();
	ms250c += ticksElapsed;

	float32 dogVelX = bodyDog->GetLinearVelocity().x;
	b2Vec2 dogPos = bodyDog->GetPosition();

	if (joyX > 10000 && dogVelX < DOG_MAX_VEL) {
		bodyDog->GetFixtureList()[0].SetFriction(0.2);
		bodyDog->ApplyLinearImpulse(b2Vec2(20, 0), dogPos);
	} else if (joyX < -10000 && dogVelX > -DOG_MAX_VEL) {
		bodyDog->GetFixtureList()[0].SetFriction(0.2);
		bodyDog->ApplyLinearImpulse(b2Vec2(-20, 0), dogPos);
	} else {
		bodyDog->GetFixtureList()[0].SetFriction(100);
	}

	world->Step(phyTimeStep, 8, 3);

	world->ClearForces();

	if (ms250c >= 250) {
		// runs every 250ms, good for debuggins

		ms250c = 0;
	}
}

void App::render() {
	glMatrixMode(GL_MODELVIEW);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glScaled(sceneXScale, sceneYScale, 1); // Scale meters to pixels

	glPushMatrix();
	glTranslated(bodyDog->GetPosition().x, bodyDog->GetPosition().y, 0); // Render onto dog's current coordinates
	glRotated(bodyDog->GetAngle() * 57.2957795, 0, 0, 1); // Spinning dog, because fuck you, that's why

	glBindTexture(GL_TEXTURE_2D, dogTexID);

	glBegin(GL_QUADS);
	// Dog is 1 meter in size
	glTexCoord2f(0, 1); glVertex3f(-0.5, -0.5, 0);
	glTexCoord2f(1, 1); glVertex3f(0.5, -0.5, 0);
	glTexCoord2f(1, 0); glVertex3f(0.5, 0.5, 0);
	glTexCoord2f(0, 0); glVertex3f(-0.5, 0.5, 0);
	glEnd();

	glPopMatrix();

	SDL_GL_SwapBuffers();
}

void App::destroy() {
	glDeleteTextures(1, &dogTexID);

	if (world) {
		delete world;
	}

	SDL_Quit();
}