void onLoop(ActorInstance@ actInst, ControlSet@ cset, float timeStep) {
    b2Vec2 actVel = actInst.bodyRef().GetLinearVelocity();
    b2Vec2 actPos = actInst.bodyRef().GetPosition();
    
    if  (actVel.x < -0.3 or actVel.x > 0.3) {
        actInst.changeState("walk");
    } else {
        actInst.changeState("stand");
    }

    if (cset !is null) {
        if (cset.x < -19000 and actVel.x > -2.0f) {
            actInst.setFriction(0.0f);
            actInst.bodyRef().ApplyLinearImpulse(b2Vec2(-20.0f, 0.0f), actPos);
        } else if (cset.x > 19000 and actVel.x < 2.0f) {
            actInst.setFriction(0.0f);
            actInst.bodyRef().ApplyLinearImpulse(b2Vec2(20.0f, 0.0f), actPos);
        } else {
            actInst.setFriction(100.0f);
        }

        if (cset.btnChange(0) and cset.btn(0)) {
            actInst.bodyRef().ApplyLinearImpulse(b2Vec2(0.0f, -70.0f), actPos);
        }
    }
}
