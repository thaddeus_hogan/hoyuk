ActorInstance@ actInst;
double xMin = 0;
double xMax = 100;

void onOffer(ActorInstance@ a) {
    if (a.name() == "testdog") {
        @actInst = @a;
        asprint("camfollowx.as Accepted ActorInstace with name " + a.name());
    }
}

void cameraLogic(Camera @camera) {
    double camXOff = camera.width() / 2;
    double newX = actInst.bodyRef().GetPosition().x - camXOff;

    if (newX < xMin) {
        camera.x(xMin);
    } else if (newX > xMax) {
        camera.x(xMax);
    } else {
        camera.x(newX);
    }
}